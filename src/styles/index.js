import grey from '@material-ui/core/colors/grey';
import blueGrey from '@material-ui/core/colors/blueGrey';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';

export const styles = theme => {
return {
	balanceBackground: {
		backgroundColor: grey[200]
	},
	balanceTitle: {
		color: blueGrey[500],
		fontWeight: 'bold'
	},
	balanceAmount: {
		color: blueGrey[700],
		fontWeight: 'bold'
	},
	balanceSub: {
		display: 'flex',
		flexDirection: 'row'
	},
	mlft20: {
		marginLeft: '20px'
	},
	greenText: {
		color: green[800]
	},
	redText: {
		color: red[500]
	},
	greenBg: {
		borderColor: green[800],
		color: green[800],
	},
	redBg: {
		borderColor: red[500],
		color: red[500],
	},
	greenDialog: {
		'& div[class*=MuiDialogTitle]': {
			backgroundColor: green[800],
			'& *[class*=MuiTypography]': {
				color: green[50]
			}
		}
	},
	redDialog: {
		'& div[class*=MuiDialogTitle]': {
			backgroundColor: red[800],
			'& *[class*=MuiTypography]': {
				color: red[50]
			}
		}
	},
	table: {
		tableLayout: 'auto'
	},

}};