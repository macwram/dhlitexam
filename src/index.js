import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch, IndexRoute } from 'react-router-dom'
import { PersistGate } from 'redux-persist/integration/react'

import CssBaseline from '@material-ui/core/CssBaseline';
import Home from './components/Home'

import reduxStore from './store'
let {store, persistor} = reduxStore()

ReactDOM.render(
	<Provider store={store}>
		<PersistGate persistor={persistor}>
			<CssBaseline />
			<Router>
				<Route path='/' key='home' component={Home} />
			</Router>
		</PersistGate>
	</Provider>,
	document.getElementById('app')
);