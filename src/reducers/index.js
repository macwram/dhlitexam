import { combineReducers } from "redux"

import balances from "./balancesReducer"

export default combineReducers({
  balances,
})
