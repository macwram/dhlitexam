export default function reducer(state={
    balances: [],
  }, action) {

    switch (action.type) {
      case 'SUBMIT_BALANCE': {
        let {id} = action.payload

        let balances = [...state.balances]
        const toUpdate = balances.findIndex(s => s.id === id)
        if(toUpdate > -1){
          balances[toUpdate] = action.payload;  
        }else{
          balances = [action.payload, ...balances]
        }

        balances = balances.sort((a,b) => a.name > b.name) 
        balances = balances.sort((a,b) => new Date(a.date) - new Date(b.date)) 
        return {
          ...state,
          balances
        }
      }
      case "DELETE_BALANCE": {
        return {
          ...state,
          balances: state.balances.filter(x => x.id !== action.payload.id),
        }
      }
    }

    return state
}
