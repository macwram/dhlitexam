import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import reducer from './reducers'

let middleware = applyMiddleware(promise(), thunk, createLogger())

const persistConfig = {
  key: 'key',
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducer)
export default () => {
  let store = createStore(persistedReducer, middleware)
  let persistor = persistStore(store)
  return { store, persistor }
}