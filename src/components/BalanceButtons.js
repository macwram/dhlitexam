import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';

import {styles} from '../styles'

class BalanceButtons extends React.Component {

	editBalance(type){
		var balance = {
			type
		}
		balance.id = Math.random().toString(36).substring(7)
		this.props.editBalance(balance)
	}

	render() {
		const { classes } = this.props;
		var title = 'Edit Balance'
		return (
			<Grid container spacing={0}>
				<Hidden smDown><Grid item md={2} lg={2}/></Hidden>
				<Grid item md={8} lg={8} xs={12} style={{ padding: 20 }}>
					<Button variant="outlined" className={classes.greenBg} onClick={() => this.editBalance('income')}>
						Add Income
					</Button>
					<Button variant="outlined" className={[classes.redBg, classes.mlft20].join(' ')} onClick={() => this.editBalance('spending')}>
						Add Spending
					</Button>
				</Grid>
				<Hidden smDown><Grid item md={2} lg={2}/></Hidden>
			</Grid>
		)
	}
}

export default withStyles(styles)(BalanceButtons)
