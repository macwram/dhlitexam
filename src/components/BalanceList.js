import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons/faPencilAlt'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons/faTrashAlt'

import {styles} from '../styles'

class BalanceList extends React.Component {
	constructor(props){
		super(props)
		this.balances = this.props.balances
		this.deleteBalance = this.deleteBalance.bind(this)
		this.editBalance = this.editBalance.bind(this)
	}
	componentWillReceiveProps(newProps){
		if(this.balances != newProps.balances){
			this.balances = newProps.balances
		}
	}

	deleteBalance(id){
		this.props.deleteBalance({id})
	}

	editBalance(balance){
		this.props.editBalance(balance)
	}

	render() {
		const { classes } = this.props;
		let balance = this.income - this.spendings
		return (
			<Grid container spacing={0}>
				<Hidden smDown><Grid item md={2} lg={2}/></Hidden>
				<Grid item md={8} lg={8} xs={12} style={{ padding: 20 }}>
					<Table className={classes.table}>
						<TableBody>
							{this.balances.map(n => {
								var amount = n.amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
								var className = n.type == 'income' ? classes.greenText : classes.redText
								return (
									<TableRow key={n.id}>
										<TableCell style={{width: '40%'}}>
											<Typography variant="caption" component="h1">{n.date}</Typography>
											<Typography variant="display1" component="h1" className={className}>{amount} Kc</Typography>
										</TableCell>
										<TableCell style={{width: '50%'}}>{n.name}</TableCell>
										<TableCell style={{width: '5%'}}>
											<IconButton onClick={() => this.editBalance(n)}>
												<FontAwesomeIcon size="xs" icon={faPencilAlt}/>
											</IconButton>
										</TableCell>
										<TableCell style={{width: '5%'}}>
											<IconButton onClick={() => this.deleteBalance(n.id)}>
												<FontAwesomeIcon size="xs" icon={faTrashAlt}/>
											</IconButton>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</Grid>
				<Hidden smDown><Grid item md={2} lg={2}/></Hidden>
			</Grid>
		)
	}
}

export default withStyles(styles)(BalanceList)
