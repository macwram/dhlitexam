import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import {styles} from '../styles'

class Balance extends React.Component {
	constructor(props){
		super(props)
		this.balances = this.props.balances
	}

	componentWillReceiveProps(newProps){
		if(this.balances != newProps.balances){
			this.balances = newProps.balances
		}
	}

	format(n){
		return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	}

	calculate(balances){
		var income = 0
		var spendings = 0
		var balance = 0
		for(let b of balances){
			if(b.type == 'income'){
				income+=b.amount
			}else{
				spendings-=b.amount
			}
		}
		balance = income + spendings
		balance = this.format(balance)
		spendings = this.format(Math.abs(spendings))
		income = this.format(income)
		return {income, spendings, balance}
	}

	render() {
		const { classes } = this.props;
		const {income, spendings, balance} = this.calculate(this.balances)
		return (
			<Grid container spacing={0} className={classes.balanceBackground}>
				<Hidden smDown><Grid item md={2} lg={2}/></Hidden>
				<Grid item md={8} lg={8} xs={12} style={{ padding: 20 }}>
					<Typography variant="subheading" component="h1" className={classes.balanceTitle}>Balance</Typography>
					<Typography variant="display3" component="h1" className={classes.balanceAmount}>{balance} CZK</Typography>
					<div className={classes.balanceSub}>
					<Typography variant="subheading" component="h1" className={[classes.balanceTitle, classes.greenText].join(' ')}>Income: {income} Kc</Typography>
					<Typography variant="subheading" component="h1" className={[classes.balanceTitle, classes.mlft20, classes.redText].join(' ')}>Spendings: {spendings} Kc</Typography>
					</div>
				</Grid>

				<Hidden smDown><Grid item md={2} lg={2}/></Hidden>
			</Grid>
		)
	}
}

export default withStyles(styles)(Balance)
