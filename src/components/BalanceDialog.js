import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import {styles} from '../styles'

class BalanceDialog extends React.Component {
	constructor(props){
		super(props)
		this.modal = this.props.modal
		this.balance = this.props.balance
		this.defaultState = {
			type: '',
			date: new Date().toISOString().split('T')[0],
			amount: '',
			name: '',
		}
		this.state = { ...{}, ...this.defaultState,  ...this.balance}
	}
	componentWillReceiveProps(newProps){
		if(this.modal != newProps.modal){
			this.modal = newProps.modal
		}
		if(this.balance != newProps.balance){
			this.balance = newProps.balance
			var bal = { ...{}, ...this.defaultState,  ...this.balance}
			this.setState(bal)
		}
	}

	close(){
		this.props.closeModal()
	}

	onChange(value, type){
		this.setState({[type]: value})
	}

	submit(){
		var {amount} = this.state
		if(amount){
			this.props.submitBalance(this.state)	
		}else{
			this.props.closeModal()
		}
		
	}


	render() {
		const { classes } = this.props;
		let { type, date, amount, name } = this.state

		var dialogTitle = 'Add Income'
		var inputProps = {endAdornment: <InputAdornment position="end">Kc</InputAdornment>}
		var className = classes.greenDialog
		if(type == 'spending'){
			inputProps['startAdornment'] = <InputAdornment position="start">-</InputAdornment>
			dialogTitle = 'Add Spending'
			className = classes.redDialog
		}
		return (
			<Dialog
				open={this.modal}
				onClose={() => this.close()}
				aria-labelledby={dialogTitle}
				className={className}
				>
				<DialogTitle id={dialogTitle}>{dialogTitle}</DialogTitle>
				<DialogContent>
					<TextField
						autoFocus
						margin="dense"
						id="amount"
						label="Amount"
						type="number"
						fullWidth
						value={amount}
						onChange={(e) => this.onChange(parseInt(e.target.value), 'amount')}
						InputProps={inputProps}
						/>
					<TextField
						margin="dense"
						id="name"
						label="Title"
						type="text"
						fullWidth
						value={name}
						onChange={(e) => this.onChange(e.target.value, 'name')}
						/>
					<TextField
						margin="dense"
						id="date"
						label="Date"
						type="date"
						fullWidth
						value={date}
						onChange={(e) => this.onChange(e.target.value, 'date')}
						/>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => this.submit()} color="primary">
						Submit
					</Button>
				</DialogActions>
			</Dialog>
		)
	}
}

export default withStyles(styles)(BalanceDialog)
