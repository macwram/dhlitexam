import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Balance from './Balance'
import BalanceList from './BalanceList'
import BalanceButtons from './BalanceButtons'
import BalanceDialog from './BalanceDialog'
import { submitBalance, deleteBalance } from '../actions/balancesActions'
import {styles} from '../styles'

class Home extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			balance: false,
			modal: false
		}
		this.balances = this.props.balances
		this.editBalance = this.editBalance.bind(this)
		this.submitBalance = this.submitBalance.bind(this)
		this.closeModal = this.closeModal.bind(this)
	}

	componentWillReceiveProps(newProps){
		if(this.balances != newProps.balances){
			this.balances = newProps.balances
		}
	}

	editBalance(balance){
		this.setState({modal: true, balance})
	}

	submitBalance(balance){
		this.props.submitBalance(balance)
		this.setState({modal: false, balance: false})
	}

	closeModal(){
		this.setState({modal:false})
	}
	
	render() {
		const { classes } = this.props;
		let {balance, modal} = this.state
		return (
			<div className={classes.root}>
				<Balance 
					balances={this.balances}
				/>
				<BalanceList
					balances={this.balances}
					deleteBalance={this.props.deleteBalance}
					editBalance={this.editBalance}
				/>
				<BalanceButtons editBalance={this.editBalance} />
				<BalanceDialog
					modal={modal}
					balance={balance}
					submitBalance={this.submitBalance}
					closeModal={this.closeModal}
					/>
			</div>
		)
	}
}

const mapStateToProps = store => {
	return {
		balances: store.balances.balances,
	}
}

const mapDispatchToProps = (dispatch,ownProps) => {
	return bindActionCreators({ submitBalance, deleteBalance }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Home))
