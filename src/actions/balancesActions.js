export function submitBalance(data){
  return function(dispatch) {
    dispatch({type: 'SUBMIT_BALANCE', payload: data})
  }
}
export function deleteBalance(data){
  return function(dispatch) {
    dispatch({type: 'DELETE_BALANCE', payload: data})
  }
}